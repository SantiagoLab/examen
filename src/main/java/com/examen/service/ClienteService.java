package com.examen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.examen.model.Cliente;
import com.examen.model.Cuenta;
import com.examen.repository.IClienteRepository;
import com.examen.repository.ICuentaRepository;

@Service
public class ClienteService implements IClienteService {

	@Autowired
	IClienteRepository iClienteRepository;
	@Autowired
	ICuentaRepository iCuentaRepository;
	
	
	@Override
	public Cliente findByIdentificacion(String identificacion) {
		return iClienteRepository.findByIdentificacion(identificacion).orElse(null);		
	}

	@Override
	public Cliente save(Cliente cliente) {
		if(!iClienteRepository.findByIdentificacion(cliente.getIdentificacion()).isPresent()) {
			
			for (Cuenta cuenta : cliente.getCuentaList()) {
				cuenta.setCliente(cliente);
			}
			
			return iClienteRepository.save(cliente);
		}
		
		return null;
	}

}
