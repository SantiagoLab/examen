package com.examen.service;

import com.examen.model.Cliente;

public interface IClienteService {
	
	Cliente findByIdentificacion(String identificacion);
	Cliente save(Cliente cliente);
}
