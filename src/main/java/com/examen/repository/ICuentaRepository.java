package com.examen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.examen.model.Cuenta;

@Repository
public interface ICuentaRepository extends JpaRepository<Cuenta, Long> {

}
