package com.examen.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.examen.model.Cliente;

@Repository
public interface IClienteRepository extends JpaRepository<Cliente, Long> {	
	
	Optional<Cliente> findByIdentificacion(@Param("identificacion") String identificacion);
}
