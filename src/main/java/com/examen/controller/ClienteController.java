package com.examen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen.model.Cliente;
import com.examen.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping()
	public ResponseEntity<?> findByIdentificacion(@RequestBody Cliente cliente) {

		String encoded = java.util.Base64.getEncoder().encodeToString(cliente.getIdentificacion().getBytes());

		cliente.setIdentificacion(encoded);

		Cliente cliente1 = clienteService.findByIdentificacion(cliente.getIdentificacion());

		if(cliente1 == null )
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No se encuentra cliente");

		return ResponseEntity.ok(cliente1);
	}

	@PostMapping()
	public ResponseEntity<?> save(@RequestBody Cliente cliente) {

		String encoded = java.util.Base64.getEncoder().encodeToString(cliente.getIdentificacion().getBytes());

		cliente.setIdentificacion(encoded);

		Cliente cliente2 = clienteService.save(cliente);
		if(cliente2 == null)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No se encuentra cliente");

		return ResponseEntity.status(HttpStatus.CREATED).body(cliente2);
	}
}
